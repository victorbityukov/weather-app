import config from 'config';
import { OpenWeatherInterface } from './interfaces';

export const OpenWeatherConfig =
  config.get<OpenWeatherInterface>('open-weather');
